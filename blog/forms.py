from django.forms import ModelForm
from .models import ArticleModel

class ArticleForm(ModelForm):
    class Meta:
        model = ArticleModel
        fields = ["titles", "category", "author","content"]